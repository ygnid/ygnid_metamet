#!/usr/bin/env python2.7

# contents.py

import os, sys
import re
from metlib.shell.fileutil import get_ext
#from datetime import datetime, timedelta
#from dateutil.parser import parse

class ContentTyper(object):
    extd = { 
            # TODO: extend this list
            '.zip':'application/zip',
            '.jpg':'application/x-jpg',
            '.jpeg':'image/jpeg',
            '.png':'application/x-png',
            '.pdf':'application/pdf',
            '.txt':'text/plain',
            '.json':'application/json',
            }

    def __call__(self, fname):
        if re.match(r'^\.[-_A-Za-z0-9]+$', fname):
            ext = fname
        else:
            ext = get_ext(fname)
        return self.extd.get(ext, 'application/octet-stream')

content_typer = ContentTyper()

if __name__ == '__main__':
    pass
