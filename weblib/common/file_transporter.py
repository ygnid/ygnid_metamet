#!/usr/bin/env python2.7

# file_transporter.py

import os, sys
import time
from metlib.misc.download import *
from metlib.shell.fileutil import *

__all__ = ['FileTransporter', 'FileTransporterBase', 'DatafileNotAvailable']

class DatafileNotAvailable(Exception):
    pass

def default_path_translator(uri, format=None):
    if format is None:
        return uri
    else:
        return uri + '.' + format

class FileTransporterBase(object):
    """
    Use this base class or not, anything is OK.
    """
    def put(self, local, uri, *args, **kwargs):
        raise NotImplementedError()

    def fetch(self, uri, dest, *args, **kwargs):
        raise NotImplementedError()

class FileTransporter(FileTransporterBase):
    """
    fetch and put file from/to remote server, or other place.
    should be used by task scheduler.
    """

    def __init__(self, remote_location, remote_type='disk', path_translator=default_path_translator, retry=3, retry_interval=10):
        self.remote_location = remote_location
        self.remote_type = remote_type
        self.path_translator = path_translator
        self.retry = retry
        self.retry_interval = retry_interval

    def put(self, local, uri, *args, **kwargs):
        pass

    def fetch(self, uri, dest, *args, **kwargs):
        if 'format' in kwargs:
            remote_sub_path = self.path_translator(uri, kwargs['format'])
        else:
            remote_sub_path = self.path_translator(uri)
        if self.remote_type == 'disk':
            retries = 1
            while retries <= self.retry:
                try:
                    remote_file_path = self.remote_location + '/' + remote_sub_path
                    CP(remote_file_path, dest)
                    return dest
                except Exception as e:
                    time.sleep(self.retry_interval)
                    retries += 1
            raise DatafileNotAvailable(uri)

        elif self.remote_type == 'http':
            try:
                remote_file_path = self.remote_location + '/' + remote_sub_path
                download_result = download(remote_file_path, dest=dest, retry=self.retry, interval=self.retry_interval)
                if download_result == False:
                    raise DatafileNotAvailable(uri)
                else:
                    return dest
            except Exception:
                raise DatafileNotAvailable(uri)

if __name__ == '__main__':
    pass
