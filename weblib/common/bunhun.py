# -*- coding:utf-8 -*-

import os
import requests
import json

POST_JSON_HEADERS = {'Content-type': 'application/json', 'Accept': 'text/plain'}

class BunHunClient(object):

    def __init__(self, name, employer):
        self.name = name
        self.employer = employer

    def req(self, career=None, careertype=None):
        data = {
            "claimer": self.name
        }
        if career is not None:
            data['career'] = career
        elif careertype is not None:
            data['careertype'] = careertype

        r = requests.post(os.path.join(self.employer, 'req'), data=json.dumps(data), headers=POST_JSON_HEADERS)
        if r.status_code == 200:
            result = json.loads(r.content, strict=False)
            if self.name == '' and result['type'] == 'Task':
                self.name = result['claimer']
            return result
        else:
            return {"type": "BadReq"}

    def reply(self, taskid, choice='AcceptTask'):
        data = {
            "type": choice,
            "claimer": self.name,
            "taskid": taskid
        }
        r = requests.post(os.path.join(self.employer, 'reply/'), json.dumps(data), headers=POST_JSON_HEADERS)
        if r.status_code == 200:
            result = json.loads(r.content, strict=False)
            return result
        else:
            return {"type": "BadReq"}

    def accept(self, taskid):
        res = self.reply(taskid=taskid, choice='AcceptTask')
        if res["type"] == "Task" and res["claimer"] == self.name and res["status"] == "Claimed":
            return True
        else:
            return False

    def reject(self, taskid):
        self.reply(taskid=taskid, choice='RejectTask')

    def report(self, taskid, status='Success', result={}):
        data = {
            "type": "ReportTask",
            "claimer": self.name,
            "taskid": taskid,
            "status": status,
            "result": result
        }
        r = requests.post(os.path.join(self.employer, 'report/'), json.dumps(data), headers=POST_JSON_HEADERS)
        if r.status_code == 200:
            return json.loads(r.content, strict=False)
        else:
            return {"type": "BadReq"}

    def goodnews(self, taskid, result={}):
        self.report(taskid, status='Success', result=result)

    def badnews(self, taskid):
        self.report(taskid, status='Failed')

    def whoami(self):
        r = requests.get(os.path.join(self.employer, 'whoami/'))
        if r.status_code == 200:
            return r.content
        else:
            return 'Unknown'


if __name__ == '__main__':
    bh = BunHunClient('test', 'http://localhost:8000/bunhun')
    # res = bh.req(career='test')
    res = bh.report('ObxYM3RxpZFgqC1e', result={'value': 1})
    print res

