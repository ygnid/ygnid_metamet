#!/usr/bin/env python2.7

# misc.py

import os, sys
#import re
#from datetime import datetime, timedelta
#from dateutil.parser import parse
#from metlib.kits import *
from django.utils import timezone

__all__ = ['local_now']

def local_now():
    return timezone.now().astimezone(timezone.get_default_timezone())

if __name__ == '__main__':
    pass
