from tastypie.authorization import DjangoAuthorization, Authorization, ReadOnlyAuthorization
from tastypie.exceptions import *

__all__ = ['UserReadOnlyAuthorization', 'UserReadWriteAuthorization',
           'DjangoAuthorization', 'Authorization', 'ReadOnlyAuthorization'
           ]

# TODO: these are far from being completed

class UserReadOnlyAuthorization(Authorization):
    def __init__(self, subtype='default'):
        self.subtype = subtype

    def read_list(self, object_list, bundle):
        if self.subtype == 'default':
            return object_list.filter(user=bundle.request.user)
        else:
            return object_list

    def read_detail(self, object_list, bundle):
        if self.subtype == 'default':
            return bundle.obj.user == bundle.request.user
        else:
            return True

    def create_list(self, object_list, bundle):
        raise Unauthorized("Sorry, not allowed.")

    def create_detail(self, object_list, bundle):
        raise Unauthorized("Sorry, not allowed.")

    def update_list(self, object_list, bundle):
        raise Unauthorized("Sorry, not allowed.")

    def update_detail(self, object_list, bundle):
        raise Unauthorized("Sorry, not allowed.")

    def delete_list(self, object_list, bundle):
        raise Unauthorized("Sorry, no deletes.")

    def delete_detail(self, object_list, bundle):
        raise Unauthorized("Sorry, no deletes.")

class UserReadWriteAuthorization(UserReadOnlyAuthorization):
    # TODO: this is really not good yet.
    def create_list(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        #return bundle.obj.user == bundle.request.user
        return True

    def update_list(self, object_list, bundle):
        return True

    def update_detail(self, object_list, bundle):
        return True

    def delete_list(self, object_list, bundle):
        return True

    def delete_detail(self, object_list, bundle):
        return True
