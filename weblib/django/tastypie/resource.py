from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned, ValidationError
from django.utils import timezone
from tastypie.resources import ModelResource
from tastypie.exceptions import *
from weblib.django.misc._all import local_now
from metlib.datetime import T, TD

__all__ = ['RacingModelResource',
           'ModelResource',
           ]

class RacingModelResource(ModelResource):
    def obj_update(self, bundle, skip_errors=False, **kwargs):
        """
        A ORM-specific implementation of ``obj_update``.
        """
        # if False or not bundle.obj or not self.get_bundle_detail_data(bundle):
        # yangdw removed the line above, to force get obj, instead of using the auto generated one.

        try:
            lookup_kwargs = self.lookup_kwargs_with_identifiers(bundle, kwargs)
        except:
            # if there is trouble hydrating the data, fall back to just
            # using kwargs by itself (usually it only contains a "pk" key
            # and this will work fine.
            lookup_kwargs = kwargs

        print lookup_kwargs

        try:
            bundle.obj = self.obj_get(bundle=bundle, **lookup_kwargs)

        except ObjectDoesNotExist:
            raise NotFound("A model instance matching the provided arguments could not be found.")


        if hasattr(bundle.obj, 'modify_dt') and 'modify_dt' in bundle.data:
            client_dt = timezone.make_aware(T(bundle.data['modify_dt']), timezone.get_default_timezone())
            server_dt = bundle.obj.modify_dt
            if client_dt < server_dt:
                raise ImmediateHttpResponse(response=HttpResponse(status=401, reason='Object Outdated'))

                # print bundle.obj
        bundle = self.full_hydrate(bundle)
        new_timestamp = local_now()
        bundle.obj.access_dt = new_timestamp
        bundle.obj.modify_dt = new_timestamp
        return self.save(bundle, skip_errors=skip_errors)
