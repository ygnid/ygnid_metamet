#!/usr/bin/env python2.7

# responses.py
import os, sys
import re
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound, \
                        HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponseForbidden, \
                        HttpResponseServerError, Http404, \
                        StreamingHttpResponse, JsonResponse

from metlib.shell.fileutil import *
from metlib.misc.misc import random_str
from weblib.common.contents import content_typer

def file_response(fname, save_name=None, content_type=None, remove_on_finish=False):
    del_tmp = False
    if re.match(r'^s3[c]?://.*', fname):
        try:
            tmpfile = '/tmp/tmp.%s%s' % (random_str(8), get_ext(fname))
            CP(fname, tmpfile)
            fname = tmpfile
            del_tmp = True
        except Exception as e:
           return HttpResponseNotFound()
    elif not os.path.exists(fname):
        return HttpResponseNotFound()

    if save_name is None:
        save_name = BASENAME(fname)
    if content_type is None:
        content_type = content_typer(fname)
    response = HttpResponse(open(fname, 'rb').read(), content_type)
    response['Content-Disposition'] = 'attachment; filename="%s"' % save_name
    if del_tmp and fname.startswith('/tmp/tmp.'):
        RM(fname)
    if remove_on_finish and os.path.isfile(fname):
        RM(fname)
    return response

class ExceptionJsonResponse(JsonResponse):
    status_code = 400

class ErrorJsonResponse(JsonResponse):
    status_code = 500

if __name__ == '__main__':
    pass
