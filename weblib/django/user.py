from django.conf import settings
from django.contrib.auth import get_user_model
import logging

__all__ = ['AUTH_USER_MODEL', 'clean_user', 'get_user_model']

logger = logging.getLogger('debug')
AUTH_USER_MODEL = settings.AUTH_USER_MODEL

def clean_user(user):
    User = get_user_model()
    if isinstance(user, User):
        if user.is_authenticated:
            return user
        else:
            return None
    elif isinstance(user, (str, unicode)):
        try:
            trueuser = User.objects.get(username=user)
        except User.DoesNotExist:
            logger.warning('Username: %s does not exist.' % user)
            return None
    elif isinstance(user, int):
        try:
            trueuser = User.objects.get(pk=user)
        except User.DoesNotExist:
            logger.warning('User ID: %d does not exist.' % user)
            return None
    else:
        logger.warning('Cannot retrieve user information from %s.' % user)
        return None
    return trueuser
