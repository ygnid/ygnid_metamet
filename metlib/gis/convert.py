#!/usr/bin/env python2.7

# convert.py

from django.contrib.gis.geos import GEOSGeometry, Point, Polygon

__all__ = [
    'lonlat_to_point_wkt', 
    'point_wkt_to_lonlat',
    'lonlat_to_rect_wkt', 
    'rect_wkt_to_lonlat',
    ]

def lonlat_to_point_wkt(lon, lat):
    point = Point(lon, lat)
    return point.wkt

def point_wkt_to_lonlat(wkt):
    point = GEOSGeometry(wkt)
    return point.x, point.y

def lonlat_to_rect_wkt(lon1, lat1, lon2, lat2):
    polygon = Polygon.from_bbox((lon1, lat1, lon2, lat2))
    return polygon.wkt

def rect_wkt_to_lonlat(wkt):
    polygon = GEOSGeometry(wkt)
    return polygon.extent

if __name__ == '__main__':
    pass
