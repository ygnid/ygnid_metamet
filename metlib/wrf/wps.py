#!/usr/bin/env python2.7

# wps.py
# FIXME: Debugging!

import os, sys
from StringIO import StringIO
import numpy as np
from matplotlib.patches import Polygon
import pyproj
from metlib.data.grid import XYGrid2D
from metlib.data.misc import where2slice
from metlib.data.boundary import Limiter

#from netCDF4 import Dataset

def parse_wps_namelist(namelist):
    """parse_wps_namelist() parses wps namelist.
    namelist:
        a namelist file or a str of (part of) namelist.

    Returns:
        a dict that contains namelist info.
    """

    if '\r' in namelist or '\n' in namelist:
        f = StringIO(namelist)
    else:
        f = open(namelist)

    result = {'default':dict() }
    current_group = 'default'
    for line in f:
        line = line.strip()
        if len(line) == 0 or line == '/':
            continue
        if line.startswith('&'):
            current_group = line[1:]
            if current_group not in result:
                result[current_group] = dict()
            continue
#        print line
        varname, values = line.split('=')
        varname = varname.strip()
        values = values.strip()
        value_list = values.split(',')
        value_list = [value.strip() for value in value_list]
        converted_value_list = []
        for value in value_list:
            if value == "":
                continue
            if (value[0] == '"' and value[-1] == '"') or \
                    (value[0] == "'" and value[-1] == "'"):
                converted_value_list.append(value[1:-1])
                continue
            try:
                converted_value = int(value)
            except ValueError:
                try:
                    converted_value = float(value)
                except ValueError:
                    converted_value = value
            finally: 
                converted_value_list.append(converted_value)
        result[current_group][varname] = converted_value_list

    return result

class Domainer(object):
    def __init__(self, namelist, extra_info={}):
        # TODO: implement things concerning extra_info
        """
        namelist: namelist filename (1 line) | namelist contents (multilines) | dict returned by parse_wps_namelist()
        extra_info: dict, which may contains:
            crop: dict of {domain_id:crop_num}
            available: list of domain_id available
            priority: list of domain_id ordered by priority
        """
        if not isinstance(namelist, dict):
            namelist = parse_wps_namelist(namelist)
        self.geogrid = namelist['geogrid']
        self.domains = range(1, len(self.geogrid['parent_id'])+1)
        self.projections = dict()
        self.truelat1 = self.geogrid['truelat1'][0]
        self.truelat2 = self.geogrid['truelat2'][0]
        self.ref_lat = self.geogrid['ref_lat'][0]
        self.ref_lon = self.geogrid['ref_lon'][0]
        self.stand_lon = self.geogrid['stand_lon'][0]
        
        if self.stand_lon != self.ref_lon:
            raise ValueError("Standard Longitude must equal Ref longitude!")

        self.dx = float(self.geogrid['dx'][0])
        self.dy = float(self.geogrid['dy'][0])
        self.earth_r_a = 6370000.0
        self.earth_r_b = 6370000.0
        self.proj = pyproj.Proj(
                proj = 'lcc',
                lat_0 = self.ref_lat,
                lon_0 = self.ref_lon,
                lat_1 = self.truelat1,
                lat_2 = self.truelat2,
                a = self.earth_r_a,
                b = self.earth_r_b,
                )

        self.domain_dx = dict()
        self.domain_dy = dict()
        # level of each domain, e.g.,
        # D1 == 27km -> level1, 
        # D2 ==  9km -> level2, 
        # Dn ==  1km -> level4, etc.
        self.sub_levels = dict()
        # consider each level as a single big grid, 
        # then each domain on that level is a part of it, with a certain ix, jy offset.
        self.sub_level_xgridnum = dict()    # key: level number
        self.sub_level_ygridnum = dict()    # key: level number
        self.sub_level_ratio = dict()       # key: level number
        self.level_domains = dict()
        self.sub_off_ix = dict()
        self.sub_off_jy = dict()
        self.xorig = dict()
        self.yorig = dict()
        self.xgridnum = dict()
        self.ygridnum = dict()

        for i in self.domains:
            self.xgridnum[i] = self.geogrid['e_we'][i-1]
            self.ygridnum[i] = self.geogrid['e_sn'][i-1]

        for i in self.domains:
            family_tree = [i]
            parent = self.geogrid['parent_id'][family_tree[-1]-1]
            while family_tree[-1] != 1:
                family_tree.append(parent)
                parent = self.geogrid['parent_id'][family_tree[-1]-1]
            family_tree.reverse()
            this_level = len(family_tree)
            self.sub_levels[i] = this_level
            if this_level not in self.level_domains:
                self.level_domains[this_level] = [i]
            else:
                self.level_domains[this_level].append(i)

            self.xorig[i] = 0.0
            self.yorig[i] = 0.0
            for member in family_tree:
                if member == 1:
                    cur_dx = self.dx
                    cur_dy = self.dy
                    self.xorig[i] -= ((self.xgridnum[member]-1) / 2.0 ) * cur_dx
                    self.yorig[i] -= ((self.ygridnum[member]-1) / 2.0 ) * cur_dy
                else:
                    self.xorig[i] += float(self.geogrid['i_parent_start'][member-1] - 1) * cur_dx 
                    self.yorig[i] += float(self.geogrid['j_parent_start'][member-1] - 1) * cur_dy 

                cur_dx /= float(self.geogrid['parent_grid_ratio'][member-1])
                cur_dy /= float(self.geogrid['parent_grid_ratio'][member-1])

            self.domain_dx[i] = cur_dx
            self.domain_dy[i] = cur_dy

            if this_level not in self.sub_level_ratio:
                ratio = int(np.round(self.dx / cur_dx))
                self.sub_level_ratio[this_level] = ratio
                self.sub_level_xgridnum[this_level] = (self.xgridnum[1] - 1) * ratio + 1
                self.sub_level_ygridnum[this_level] = (self.ygridnum[1] - 1) * ratio + 1

            self.sub_off_ix[i] = 0
            self.sub_off_jy[i] = 0
            for member in family_tree:
                level = self.sub_levels[member]
                parent_ratio = self.geogrid['parent_grid_ratio'][member-1]
                self.sub_off_ix[i] = (self.sub_off_ix[i] + self.geogrid['i_parent_start'][member-1] - 1) * parent_ratio
                self.sub_off_jy[i] = (self.sub_off_jy[i] + self.geogrid['j_parent_start'][member-1] - 1) * parent_ratio
                
        self.dot_grids = dict()
        self.cross_grids = dict()
        self.excross_grids = dict()
        for i in self.domains:
            dot_IX, dot_JY = np.meshgrid(np.arange(self.xgridnum[i]), np.arange(self.ygridnum[i]))
            cross_IX, cross_JY = np.meshgrid(np.arange(self.xgridnum[i]-1), np.arange(self.ygridnum[i]-1))
            excross_IX, excross_JY = np.meshgrid(np.arange(-1, self.xgridnum[i]), np.arange(-1, self.ygridnum[i]))
            dot_lons, dot_lats = self.ij_to_lonlat(dot_IX, dot_JY, domain=i, dot=True)
            cross_lons, cross_lats = self.ij_to_lonlat(cross_IX, cross_JY, domain=i, dot=False)
            excross_lons, excross_lats = self.ij_to_lonlat(excross_IX, excross_JY, domain=i, dot=False)
            self.dot_grids[i] = XYGrid2D(dot_lons, dot_lats)
            self.cross_grids[i] = XYGrid2D(cross_lons, cross_lats)
            self.excross_grids[i] = XYGrid2D(excross_lons, excross_lats)

        self.dot_x_limiters = dict()
        self.dot_y_limiters = dict()
        self.cross_x_limiters = dict()
        self.cross_y_limiters = dict()
        for i in self.domains:
            self.dot_x_limiters[i] = Limiter(0, self.xgridnum[i]-1)
            self.dot_y_limiters[i] = Limiter(0, self.ygridnum[i]-1)
            self.cross_x_limiters[i] = Limiter(0, self.xgridnum[i]-1-1)
            self.cross_y_limiters[i] = Limiter(0, self.ygridnum[i]-1-1)

    def lonlat_to_xy(self, lon, lat):
        return self.proj(lon, lat)

    def lonlat_to_ij(self, lon, lat, domain=1, dot=False, int_index=False):
        domain = int(domain)
        x, y = self.lonlat_to_xy(lon, lat)
        i = (x - self.xorig[domain]) / self.domain_dx[domain]
        j = (y - self.yorig[domain]) / self.domain_dy[domain]
        domain = int(domain)
        if not dot:
            i = i - 0.5
            j = j - 0.5
        if int_index:
            if np.isscalar(i):
                i = int(np.round(i))
                j = int(np.round(j))
            else:
                i = np.round(i).astype('i')
                j = np.round(j).astype('i')
        return i, j

    def lonlat_to_level_ij(self, lon, lat, level=1, dot=False, int_index=False):
        d = self.level_domains[level][0]
        i, j = self.lonlat_to_ij(lon, lat, domain=d, dot=dot, int_index=int_index)
        i += self.sub_off_ix[d]
        j += self.sub_off_jy[d]
        return i, j
    
    def xy_to_lonlat(self, x, y):
        return self.proj(x, y, inverse=True)

    def ij_to_xy(self, i, j, domain=1, dot=False):
        domain = int(domain)
        if not dot:
            i = i + 0.5
            j = j + 0.5
        x = i * self.domain_dx[domain] + self.xorig[domain]
        y = j * self.domain_dy[domain] + self.yorig[domain]
        return x, y

    def ij_to_lonlat(self, i, j, domain=1, dot=False):
        domain = int(domain)
        x, y = self.ij_to_xy(i, j, domain=domain, dot=dot)
        return self.xy_to_lonlat(x, y)

    def level_ij_to_lonlat(self, i, j, level=1, dot=False):
        d = self.level_domains[level][0]
        i -= self.sub_off_ix[d]
        j -= self.sub_off_jy[d]
        lon, lat = self.ij_to_lonlat(i, j, domain=d, dot=dot)
        return lon, lat

    def lonlat_rect_to_ij_slice(self, lon1, lat1, lon2, lat2, domain=1, dot=False, return_mask=False, domain_border_trim=0):
        lonmin = min(lon1, lon2); lonmax = max(lon1, lon2)
        latmin = min(lat1, lat2); latmax = max(lat1, lat2) 
        domain = int(domain)
        expand_size = self.domain_dx[domain] / 50.0
        lonmin -= expand_size; lonmax += expand_size
        latmin -= expand_size; latmax += expand_size
        dbtrim = domain_border_trim
        if dot:
            grid = self.dot_grids[domain]
            grid_outer = self.excross_grids[domain]
            goyn, goxn = grid_outer.X.shape
            good_lons = (grid_outer.X[dbtrim:-1-dbtrim, dbtrim:-1-dbtrim] >= lonmin) & (grid_outer.X[1+dbtrim:goyn-dbtrim, 1+dbtrim:goxn-dbtrim] <= lonmax)
            good_lats = (grid_outer.Y[dbtrim:-1-dbtrim, dbtrim:-1-dbtrim] >= latmin) & (grid_outer.Y[1+dbtrim:goyn-dbtrim, 1+dbtrim:goxn-dbtrim] <= latmax)
        else:
            grid = self.cross_grids[domain]
            grid_outer = self.dot_grids[domain]
            goyn, goxn = grid_outer.X.shape
            good_lons = (grid_outer.X[1+dbtrim:goyn-dbtrim, 1+dbtrim:goxn-dbtrim] >= lonmin) & (grid_outer.X[dbtrim:-1-dbtrim, dbtrim:-1-dbtrim] <= lonmax)
            good_lats = (grid_outer.Y[1+dbtrim:goyn-dbtrim, 1+dbtrim:goxn-dbtrim] >= latmin) & (grid_outer.Y[dbtrim:-1-dbtrim, dbtrim:-1-dbtrim] <= latmax)
        good_lons_slice = where2slice(np.where(good_lons.sum(axis=0)))
        good_lats_slice = where2slice(np.where(good_lats.sum(axis=1)))
        # TODO: now using grid center points to judge, not strict on the edge.
        part_X = grid.X[good_lats_slice, good_lons_slice]
        part_Y = grid.Y[good_lats_slice, good_lons_slice]

        corner_path = Polygon([
            [lonmin, latmin],
            [lonmax, latmin],
            [lonmax, latmax],
            [lonmin, latmax]
            ]).get_path()
        xys = np.array([part_X.flatten(), part_Y.flatten()]).transpose()
        mask = corner_path.contains_points(xys).reshape(part_X.shape)
        w_really_good = np.where(mask)
        if len(w_really_good[0]) == 0:
            really_good_lons_slice = slice(0)
            really_good_lats_slice = slice(0)
            small_mask = np.array([[]], dtype=bool)
        else:
            small_mask = mask[np.min(w_really_good[0]):np.max(w_really_good[0])+1, np.min(w_really_good[1]):np.max(w_really_good[1])+1]
        
            really_good_lons_slice = slice(np.min(w_really_good[1])+good_lons_slice.start, np.max(w_really_good[1])+1+good_lons_slice.start)
            really_good_lats_slice = slice(np.min(w_really_good[0])+good_lats_slice.start, np.max(w_really_good[0])+1+good_lats_slice.start)

        if return_mask:
            return really_good_lons_slice, really_good_lats_slice, small_mask
        else:
            return really_good_lons_slice, really_good_lats_slice

    def lonlat_rect_to_merge_info(self, lon1, lat1, lon2, lat2, level=1, 
                dot=False, domain_border_trim=0):
        good_domains = []
        offed_ix_start = []
        offed_ix_stop = []
        offed_jy_start = []
        offed_jy_stop = []
        for domain in self.level_domains[level]:
            s_ix, s_jy, mask = self.lonlat_rect_to_ij_slice(lon1, lat1, lon2, lat2, 
                    domain=domain, dot=dot, return_mask=True, domain_border_trim=domain_border_trim)
            if s_ix.stop != 0 and s_jy.stop != 0:
                off_ix = self.sub_off_ix[domain] 
                off_jy = self.sub_off_jy[domain]
                s_ix_offed = slice(s_ix.start+off_ix, s_ix.stop+off_ix, s_ix.step)
                s_jy_offed = slice(s_jy.start+off_jy, s_jy.stop+off_jy, s_ix.step)
                offed_ix_start.append(s_ix_offed.start)
                offed_ix_stop.append(s_ix_offed.stop)
                offed_jy_start.append(s_jy_offed.start)
                offed_jy_stop.append(s_jy_offed.stop)
                good_domains.append((domain, s_ix, s_jy, s_ix_offed, s_jy_offed, mask))
        if len(good_domains) == 0:
            return None
        level_s_ix = slice(np.min(offed_ix_start), np.max(offed_ix_stop))
        level_s_jy = slice(np.min(offed_jy_start), np.max(offed_jy_stop))
        level_total_ix = range(self.sub_level_xgridnum[level] - int(not dot))
        level_total_jy = range(self.sub_level_ygridnum[level] - int(not dot))
        level_part_ix = level_total_ix[level_s_ix]
        level_part_jy = level_total_jy[level_s_jy]
        level_part_IX, level_part_JY = np.meshgrid(level_part_ix, level_part_jy)
        level_part_LON, level_part_LAT = self.level_ij_to_lonlat(level_part_IX, level_part_JY, level=level, dot=dot)
        part_off_ix = level_s_ix.start
        part_off_jy = level_s_jy.start

        result = dict()
        result['LON'] = level_part_LON
        result['LAT'] = level_part_LAT
        result['level_ixs'] = level_part_ix
        result['level_jys'] = level_part_jy
        result['domain_s_ix'] = dict()
        result['domain_s_jy'] = dict()
        result['level_s_ix'] = dict()
        result['level_s_jy'] = dict()
        result['domain_mask'] = dict()
        result['domains'] = []
        for domain, s_ix, s_jy, s_ix_offed, s_jy_offed, mask in good_domains:
            result['domains'].append(domain)
            s_ix_offoff = slice(s_ix_offed.start-part_off_ix, s_ix_offed.stop-part_off_ix, s_ix_offed.step)
            s_jy_offoff = slice(s_jy_offed.start-part_off_jy, s_jy_offed.stop-part_off_jy, s_jy_offed.step)
            result['domain_s_ix'][domain] = s_ix
            result['domain_s_jy'][domain] = s_jy
            result['level_s_ix'][domain] = s_ix_offoff
            result['level_s_jy'][domain] = s_jy_offoff
            result['domain_mask'][domain] = mask
        return result
        
    def ij_in_domain(self, i, j, domain=1, dot=False):
        domain = int(domain)
        i_max = self.xgridnum[domain]
        j_max = self.ygridnum[domain]
        if not dot:
            i_max -= 1
            j_max -= 1
        return ((i >= 0) & (i < i_max)) & ((j >= 0) & (j < j_max))

    def lookup_lonlat(self, lon, lat, dot=False, int_index=False):
        """find which domain contains the given lon lat, and return a list of dicts like:
        {
            "domain": DOMAIN,
            "resolution": "NNkm",
            "jy": JY,
            "ix": IX,
            "grid_lon":
            "grid_lat":
        }
        """
        res_list = []
        for domain in self.domains:
            i, j = self.lonlat_to_ij(lon, lat, domain=domain, dot=dot, int_index=int_index)
            if self.ij_in_domain(i, j, domain):
                grid_lon, grid_lat = self.ij_to_lonlat(i, j, domain=domain, dot=dot)
                res = {
                    'domain': domain,
                    'resolution': '%gkm' % (float(self.domain_dx[domain]) / 1000.0),
                    'jy': j,
                    'ix': i,
                    'grid_lon': grid_lon,
                    'grid_lat': grid_lat
                }
                res_list.append(res)
        return res_list  # TODO

    def lookup_lonlat_rect(self, lon1, lat1, lon2, lat2, dot=False, by='domain_level', domain_border_trim=0, array_format='np.array'):
        """
        by: 'domain_level', 'domain'.
        returns a list of dicts like:
        {
            "domain": DOMAIN,
            "resolution": ,

        }
        """
        res_list = []
        if by == 'domain_level':
            for dlevel, domains in self.level_domains.iteritems():
                merge_info = self.lonlat_rect_to_merge_info(lon1, lat1, lon2, lat2, level=dlevel, dot=dot, domain_border_trim=domain_border_trim)
                if len(merge_info['domains']) == 0:
                    return
                example_domain = merge_info['domains'][0]
                resolution = '%gkm' % (float(self.domain_dx[example_domain]) / 1000.0)
                ixs = merge_info['level_ixs']
                jys = merge_info['level_jys']

                lat = merge_info['LAT']
                lon = merge_info['LON']
                if array_format == 'list':
                    lat = lat.tolist()
                    lon = lon.tolist()

                res = {
                    "type": "domain_level",
                    "domain": "L%s" % dlevel,
                    "resolution": resolution,
                    "jy1": jys[0],
                    "ix1": ixs[0],
                    "jy2": jys[-1]+1,
                    "ix2": ixs[-1]+1,
                    "grid_lon": lon,
                    "grid_lat": lat,
                    "subs": []
                }

                for domain in merge_info['domains']:
                    sub_ixs = merge_info['domain_s_ix'][domain]
                    sub_jys = merge_info['domain_s_jy'][domain]
                    mask = merge_info['domain_mask'][domain]
                    if array_format == 'list':
                        mask = mask.astype('i1').tolist()

                    subres = {
                        "type": "domain",
                        "domain": domain,
                        "resolution": resolution,
                        "jy1": sub_jys.start,
                        "ix1": sub_ixs.start,
                        "jy2": sub_jys.stop,
                        "ix2": sub_ixs.stop,
                        "jy_offset": self.sub_off_jy[domain],
                        "ix_offset": self.sub_off_ix[domain],
                        "mask": mask
                    }
                    res['subs'].append(subres)

                res_list.append(res)
            return res_list

    def level_region_to_domain_regions(self, level, ix1, ix2, jy1, jy2, dot=False):
        level = int(level)
        res_list = []
        if dot:
            x_limiters = self.dot_x_limiters
            y_limiters = self.dot_y_limiters
        else:
            x_limiters = self.cross_x_limiters
            y_limiters = self.cross_y_limiters

        # TODO: domain priority.
        # TODO: border_trim
        for domain in self.level_domains[level]:
            ix_offset = self.sub_off_ix[domain]
            jy_offset = self.sub_off_jy[domain]

            domain_ix_slice = x_limiters[domain][ix1-ix_offset:ix2-ix_offset]
            domain_jy_slice = y_limiters[domain][jy1-jy_offset:jy2-jy_offset]

            if domain_ix_slice.stop <= domain_ix_slice.start or \
                    domain_jy_slice.stop <= domain_jy_slice.start :
                continue

            res = {
                "domain": domain,
                "ix1": domain_ix_slice.start,
                "ix2": domain_ix_slice.stop,
                "jy1": domain_jy_slice.start,
                "jy2": domain_jy_slice.stop,
                "ix_offset": ix_offset,
                "jy_offset": jy_offset,
            }

            res_list.append(res)

        return res_list


if __name__ == '__main__':
    test_nl = """
&share
 wrf_core = 'ARW',
 max_dom = 2,
 start_date = '2008-03-24_12:00:00','2008-03-24_12:00:00',
 end_date   = '2008-03-24_18:00:00','2008-03-24_12:00:00',
 interval_seconds = 21600,
 io_form_geogrid = 2
/

&geogrid
 parent_id         =   1,   1,  2,
 parent_grid_ratio =   1,   3,  3,
 i_parent_start    =   1,  31,  20,
 j_parent_start    =   1,  17,  15,
 s_we              =   1,   1, 1
 e_we              =  74, 112, 100,
 s_sn              =   1,   1, 1,
 e_sn              =  61,  97, 80,
 geog_data_res     = '10m','2m','30s'
 dx = 30000,
 dy = 30000,
 map_proj = 'lambert',
 ref_lat   = 34.83,
 ref_lon   = -81.03,
 truelat1  =  30.0,
 truelat2  =  60.0,
 stand_lon = -98.,
 geog_data_path = '/mmm/users/wrfhelp/WPS_GEOG/'
/
"""

    res = parse_wps_namelist(test_nl)

    print res

    dmr = Domainer(res)

