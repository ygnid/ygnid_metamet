# -*- coding:utf-8 -*-

__all__ = ['DatasetBase', 'DatasetMoldBase', 'parse_dataset_uri_head']

import copy
from collections import OrderedDict

def parse_dataset_uri_head(uri):
    """parse a uri's head (dataset part) into top dataset_uri and subset_uri
    """
    head = uri.split('/', 1)[0]
    spls = head.split(':', 1)
    dataset_uri = ''
    subset_uri = ''
    if len(spls) >= 1:
        dataset_uri = spls[0]
    if len(spls) == 2:  # TODO: recheck the logic
        subset_uri = spls[1]
    return dataset_uri, subset_uri

class DatasetBase(object):
    varname_translate_dict = {}
    desc = ''
    accept_tags = set([])
    # TODO: check which info are necessary, leave the rest to the individual datasets.

    def __init__(self, name, uri, varnames=[], parent=None, *args, **kwargs):
        self.name = name
        self.uri = uri

        self._varnames = varnames

        self._parent = parent
        self._subsets = {}

    @property
    def hierarchical_name(self):
        if self._parent is None:
            return self.name
        else:
            return self._parent.hierarchical_name + ':' + self.name

    @property
    def hierarchical_uri(self):
        if self._parent is None:
            return self.uri
        else:
            return self._parent.hierarchical_uri + ':' + self.uri

    @property
    def varnames(self):
        res = self._varnames[:]
        for subset in self.subsets.values():
            subvarnames = [subset.uri + '/' + vn for vn in subset.varnames]
            res.extend(subvarnames)
        return res

    @property
    def subsets(self):
        return self._subsets

    def add_subset(self, subset):
        to_append = subset.copy()
        to_append._parent = self
        self._subsets[subset.name] = to_append

    def copy(self):
        return copy.deepcopy(self)

    def get_data(self, meta_only=False, *args, **kwargs):
        results = []
        for subset in self.subsets.values():
            try:
                sub_res = subset.get_data(meta_only=meta_only, *args, **kwargs)
                results.extend(sub_res)
            except Exception as e:
                raise e
        return results

    def get_briefs(self, tags=['*'], *args, **kwargs):
        results = []
        common_tags = set(tags) & set(self.accept_tags)
        if len(common_tags) == 0:
            return results

        for subset in self.subsets.values():
            try:
                sub_res = subset.get_briefs(tags=tags, *args, **kwargs)
                if isinstance(sub_res, list):
                    results.extend(sub_res)
                else:
                    results.append(sub_res)
            except Exception as e:
                raise e
        return results

    def get_schemas(self, tags=['*'], *args, **kwargs):
        results = []
        common_tags = set(tags) & set(self.accept_tags)
        if len(common_tags) == 0:
            return results

        for subset in self.subsets.values():
            try:
                sub_res = subset.get_schemas(tags=tags, *args, **kwargs)
                if isinstance(sub_res, list):
                    results.extend(sub_res)
                else:
                    results.append(sub_res)
            except Exception as e:
                raise e
        return results

    def get_info_by_uri(self, uri):
        raise NotImplementedError()

    def get_egg_by_uri(self, uri):
        raise NotImplementedError()

    def get_data_by_uri(self, uri):
        raise NotImplementedError()

    def get_file_by_uri(self, uri, dest, format='default', sample=False):
        raise NotImplementedError()

class DatasetMoldBase(object):
    # list of dicts like {'name':, 'uri':, 'class':}
    subset_classes = []

    @classmethod
    def get_schema(cls):
        raise NotImplementedError

    @classmethod
    def get_mold_desc(cls):
        return ''

    def __init__(self, name, uri, info, *args, **kwargs):
        self.name = name
        self.uri = uri
        self.info = info

        self._parent = None
        self._subsets = OrderedDict()
        self._uri_subsets = OrderedDict()
        for item in self.subset_classes:
            subset = item['class'](item['name'], item['uri'], info, *args, **kwargs)
            self.add_subset(subset)


    @property
    def hierarchical_name(self):
        if self._parent is None:
            return self.name
        else:
            return self._parent.hierarchical_name + '/' + self.name

    @property
    def hierarchical_uri(self):
        if self._parent is None:
            return self.uri
        else:
            return self._parent.hierarchical_uri + '/' + self.uri

    @property
    def subsets(self):
        return self._subsets

    @property
    def uri_subsets(self):
        return self._uri_subsets

    def add_subset(self, subset, copy=False):
        to_append = subset.copy() if copy else subset
        to_append._parent = self
        self._subsets[subset.name] = to_append
        self._uri_subsets[subset.uri] = to_append

    def copy(self):
        return copy.deepcopy(self)

    def lookup(self, *args, **kwargs):
        raise NotImplementedError

    def get_data(self, uri, *args, **kwargs):
        raise NotImplementedError
