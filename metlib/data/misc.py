#!/usr/bin/env python2.7

# rolling_mean.py

import os, sys
#import re
from datetime import datetime, timedelta
#from dateutil.parser import parse
import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
#from mpl_toolkits.basemap import Basemap
#from matplotlib import mlab
#from netCDF4 import Dataset

__all__ = ['nearest_i', 'where2slice', 'where2slices', 'seq2parts', 'seq2chunks', 'parse_slice_str']

def nearest_i(arr, target):
    """return the nearest index for target in arr.
    Supporting only 1D data for now.
    """
    dists = np.abs(np.array(arr) - target)
    if isinstance(dists[0], timedelta):
        dists = np.array([td.total_seconds() for td in dists])
    return np.nanargmin(dists)

def where2slice(w):
    """convert np.where() results to slice if possible.
    """
    if len(w) != 1:
        raise ValueError("where2slice supports only 1D where results, given w is %d" % len(w))

    if len(w[0]) == 0:
        return slice(0, 0, 1)
    elif len(w[0]) == 1:
        return slice(w[0][0], w[0][0] + 1, 1)
    else:
        step = w[0][1] - w[0][0]
        start = w[0][0]
        end = w[0][-1] + step
        #TODO: check match
        return slice(start, end, step)

def where2slices(w):
    """convert np.where() results to a list of slices.
    """
    if len(w) != 1:
        raise ValueError("where2slice supports only 1D where results, given w is %d" % len(w))

    if len(w[0]) == 0:
        return []
    elif len(w[0]) == 1:
        return [slice(w[0][0], w[0][0] + 1, 1)]
    else:
        result = []
        cur_start = w[0][0]
        cur_step =  w[0][1] - w[0][0]
        last = w[0][0]
        for now in w[0][1:]:
            if cur_step is None:
                cur_step = now - last
                last = now
            elif now - last == cur_step:
                last = now
            else:
                result.append(slice(cur_start, last+cur_step, cur_step))
                cur_start = now
                last = now
                cur_step = None
        if cur_step is None:
            result.append(slice(cur_start, cur_start+result[0].step, result[0].step))
        elif last != cur_start:
            result.append(slice(cur_start, last+cur_step, cur_step))
        return result

def seq2chunks(l, n):
    """ Yield successive n-sized chunks from l.
    by: Ned Batchelder
    http://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks-in-python
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def seq2parts(seq):
    """break a seq to a list of parts, guaranteeing that each part continuous and the interval is even.
    """
    if len(seq) == 0:
        return []
    elif len(seq) == 1:
        return [[seq[0]]]
    else:
        result = []
        cur_start = seq[0]
        cur_step = seq[1] - seq[0]
        last = seq[0]
        cur_list = [seq[0]]
        for now in seq[1:]:
            if cur_step is None:
                cur_step = now - last
                cur_list.append(now)
                last = now
            elif now - last == cur_step:
                cur_list.append(now)
                last = now
            else:
                result.append(cur_list)
                cur_list = [now]
                cur_start = now
                last = now
                cur_step = None
        if cur_step is None or last != cur_start:
            result.append(cur_list)
        return result

def parse_slice_str(s, default_start='0', default_step='1', sep=':', converter=None):
    """ parse slice str into start, stop, step
    :param s: string to be parsed
    :param default_start:
    :param default_step:
    :param converter: if not None, convert the result with the converter.
    :return: (start, stop, step)
    """
    tokens = s.split(sep)
    if len(tokens) == 1:
        start = default_start
        stop = tokens[0]
        step = default_step
    elif len(tokens) == 2:
        start = tokens[0]
        stop = tokens[1]
        step = default_step
    elif len(tokens) >= 3:
        start = tokens[0]
        stop = tokens[1]
        step = tokens[2]

    if converter is not None:
        start = converter(start)
        stop = converter(stop)
        step = converter(step)

    return start, stop, step

if __name__ == '__main__':
    from metlib.kits import *
    dts = datetime_range(20140101, 20140201, '1d')
    dt = T(20140107)
    print nearest_i(dts, dt)
