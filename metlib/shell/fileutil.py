#!/usr/bin/env python
import os
from datetime import datetime
from glob import glob
import fnmatch
import re
import shutil
from metlib.shell.script_helper import get_output
from metlib.misc.multiprocess import split_job
try:
    import boto
    import boto.s3
    _has_boto = True
except ImportError:
    _has_boto = False

__all__ = ['filesize', 'sorted_walk', 'list_all_file',  
            'force_rm', 'force_makedirs', 'expand_path', 'CB', 'get_rel_path', 'find_link_orig', 
            'strip_ext', 'sub_ext', 'get_ext', 
            'LS', 'LS_R', 'CD', 'P', 'DIRNAME', 'BASENAME', 
            'RM', 'CP', 'batch_cp', 'MKDIR', 'MV', 'DU']

def filesize(f):
    """Return the size of f in bytes"""
    if isinstance(f, file):
        now_pos = f.tell()
        f.seek(0, 2)
        size = f.tell()
        f.seek(now_pos)
        return size
    else:
        return os.stat(f).st_size

def sorted_walk(top, **kwarg):
    """returns the sorted result in a list consisting os.walk's 3-tuple: (dirpath, dirnames, filename). 
    kwargs: os.path's kwargs, i.e. topdown=True[, onerror=None[, followlinks=False]]"""
    if 'followlinks' not in kwarg:
        kwarg['followlinks'] = True
    w = os.walk(top, **kwarg)
    res = list(w)
    res.sort()
    for stuff in res:
        stuff[1].sort()
        stuff[2].sort()
    return res

def list_all_file(top='.', fname_pattern=r'.*', dir_pattern=r'.*', \
                    ignore_hidden=True, recursive=True, base_only=False, **kwarg):
    """returns a list of filenames that matches the 2 regex patterns, 
    kwargs: os.path's kwargs, i.e. topdown=True[, onerror=None[, followlinks=True]]"""
    try:
        newtop = os.path.expanduser(os.path.expandvars(top))
    except Exception:
        newtop = top

    if newtop != '/' and newtop.endswith('/'):
        newtop = newtop[:-1]

    if not os.path.exists(newtop):
        return []
    if 'followlinks' not in kwarg:
        kwarg['followlinks'] = True
    if recursive:
        walktuplelist = sorted_walk(newtop, **kwarg)
    else:
        fnames = sorted(os.listdir(newtop))
        walktuplelist = [('', [], fnames)]
    filelist = []
    for walktuple in walktuplelist:
        d = walktuple[0]
        if re.search(dir_pattern, d):
            if ignore_hidden:
                sorted_dirs = sorted(d.split(os.path.sep))
                sorted_dirs = [subd for subd in sorted_dirs if subd not in ('', '.', '..')]
                if len(sorted_dirs) > 0 and sorted_dirs[0].startswith('.'):
                    continue
            for fname in walktuple[2]:
                if re.search(fname_pattern, fname):
                    if ignore_hidden:
                        if fname.startswith('.') or fname.endswith('~'):
                            continue
                    filelist.append(os.path.join(d, fname))
    if base_only:
        trim_n = len(newtop) + 1
        filelist = [fn[trim_n:] for fn in filelist]
    return filelist

def LS(top='.', *args, **kwargs):
    """wrapping for list_all_file() with recursive default as False.
    kwargs are the same as list_all_file, i.e.,
        fname_pattern, dir_pattern, ignore_hidden, recursive, followlinks, etc.
    """
    recursive = kwargs.pop('recursive', False)
    return list_all_file(top, *args, recursive=recursive, **kwargs)
    
LS_R = list_all_file

def CD(path=None):
    """CD changes dir
    """
    if path is None:
        path = P('~')
    else:
        path = P(path)
    return os.chdir(path)

def expand_path(path):
    """expand ~ and $ in the path"""
    return os.path.expanduser(os.path.expandvars(path))

def CB(tag):
    """Using the metamet/shell/cb (short for clipboard) to get recorded text snippets, e.g., long file names, etc."""
    return get_output('cb %s' % tag)

# def force_rm(fname, regex=False):
#     """force to rm fname, no matter whether fname is file, dir or link"""
#     # TODO, add regex support. and make it more robust.
#     fnames = glob(fname)
#     for fn in fnames:
#         try:
#             if os.path.islink(fn):
#                 os.unlink(fn)
#             elif os.path.isdir(fn):
#                 shutil.rmtree(fn)
#             else:
#                 os.remove(fn)
#         except Exception as e:
#             print e

def force_makedirs(dirname, rm_exist_dir=False):
    """force to make dir, no matter whether it exists"""
    #TODO: make it more robust
    if dirname in ['', '.', '..', './', '../', '/']:
        return
    orig = dirname
    if os.path.islink(orig):
        orig = find_link_orig(dirname)
    if os.path.isfile(orig):
        if not rm_exist_dir:
            raise RuntimeError('Cannot makedirs: %s is file' % dirname)
    if rm_exist_dir:
        try:
            force_rm(dirname)
        except Exception as e:
            pass
    try:
        os.makedirs(dirname)
    except OSError as e:
        if e.errno != 17:
            raise e

def get_rel_path(path, base):
    """get relative path, e.g., get_rel_path('abc/de/fg', 'abc') => 'de/fg'
    """
    lb = len(base)
    assert path[:lb] == base
    if len(path) == lb:
        rel_path = ''
    elif path[lb] == '/':
        rel_path = path[lb+1:]
    else:
        rel_path = path[lb:]
    return rel_path

def find_link_orig(path, max_depth=99):
    """Try to find the orig of a link."""
    count = 0
    while count < max_depth:
        if os.path.islink(path):
            path = os.readlink(path)
        else:
            return path
        count += 1
    return path

P = expand_path
# RM = force_rm
MKDIR = force_makedirs
DIRNAME = os.path.dirname
BASENAME = os.path.basename
MV = shutil.move

class FileOperator(object):
    def __init__(self):
        if _has_boto:
            self._s3_cache = {}
            self._bucket_cache = {}

    def parse_path(self, path, srcdest='src'):
        info = {}
#        if re.match(r'^https?://.*', path):
#            loc_type = 'http'  #TODO: implement
        if re.match(r'(^arn:aws:s3:::.*$)|(^s3://.*$)', path):
            loc_type = 's3'
        elif re.match(r'(^arn:aws-cn:s3:::.*$)|(^s3c://.*$)', path):
            loc_type = 's3c'
        # TODO: add ftp
        # TODO: file and link 
        else: 
            loc_type = 'fs'

        path_type = 'file'
        if path.endswith('/'):
            path_type = 'dir'
        
        if loc_type in ('s3', 's3c'):
            m = re.match(r'^((arn:aws[^:]*:s3:::)|(s3c?://))(?P<bucketname>[^/]+)/(?P<keyname>.*)', path)
            if not m:
                raise ValueError('Bad aws s3 path: %s' % path)
            bucketname = m.group('bucketname')
            keyname = m.group('keyname')
            
            s3 = self._s3_cache.get(loc_type, None)
            if s3 is None:
                if loc_type == 's3':
                    s3 = boto.connect_s3()
                else:
                    s3 = boto.s3.connect_to_region('cn-north-1')
                if s3 is None:
                    raise RuntimeError('Cannot connect to aws s3')
                self._s3_cache[loc_type] = s3
            bucket = self._bucket_cache.get((loc_type, bucketname), None)
            if bucket is None:
                bucket = s3.get_bucket(bucketname)
                if bucket is None:
                    if srcdest == 'src':
                        raise ValueError('Aws s3 bucket does not exist: %s %s' % (loc_type, bucketname))
                    else:
                        location = 'cn-north-1' if loc_type == 's3c' else None
                        bucket = s3.create_bucket(bucketname, location=location)
                self._bucket_cache[(loc_type, bucketname)] = bucket

            if srcdest == 'src':
                key = bucket.get_key(keyname)
                if (path_type == 'file' and key is None) or path_type == 'dir':
                    keys = [key for key in bucket.list(keyname) if not key.key.endswith('/')]
                    if len(keys) == 0:
                        raise ValueError('Aws s3 folder does not exist: %s' % path)
                    path_type = 'dir'
                    info['keys'] = keys
                else:
                    info['key'] = key

            info['bucketname'] = bucketname
            info['keyname'] = keyname
            info['bucket'] = bucket

        if loc_type in ('fs',):
           if srcdest == 'src':
               if os.path.isdir(path):
                   path_type = 'dir'
           if srcdest == 'dest':
               true_dest = find_link_orig(path)
               if os.path.isdir(true_dest):
                   path_type = 'dir'
                   info['true_dest'] = true_dest

        return loc_type, path_type, info

    def cp(self, src, dest, symlinks=True, ignore_hidden=True, checksize=False):
        src = expand_path(src)
        dest = expand_path(dest)
        src_loc_type, src_path_type, src_info = self.parse_path(src, srcdest='src')
        dest_loc_type, dest_path_type, dest_info = self.parse_path(dest, srcdest='dest')

        if src_loc_type == 'fs' and dest_loc_type == 'fs':
            if src_path_type == 'file':
                if dest_path_type == 'dir':
                    dest = os.path.join(dest, BASENAME(src))
                    if checksize and (filesize(src) == filesize(dest)):
                        return 
                else:
                    if checksize and (filesize(src) == filesize(dest)):
                        return
                    RM(dest)
                    MKDIR(DIRNAME(dest))
                return shutil.copy(src, dest)
            else:
                if dest_path_type == 'dir':
                    dest = os.path.join(dest, BASENAME(src))
                    RM(dest)
                else:
                    if os.path.exists(dest):
                       raise ValueError('Cannot copy a dir into a single file: %s' % dest)

                if ignore_hidden:
                    hidden_func = lambda s, ns: [n for n in ns if n.startswith('.')]
                else:
                    hidden_func = None
                return shutil.copytree(src, dest, symlinks=symlinks, ignore=hidden_func)
                
        if src_loc_type == 'fs' and dest_loc_type in ('s3', 's3c'):
            if src_path_type == 'file':
                # TODO: add filesize check
                if dest_path_type == 'dir':
                    dest_keyname = os.path.join(dest_info['keyname'], BASENAME(src))
                else:
                    dest_keyname = dest_info['keyname']
                dest_key = dest_info['bucket'].new_key(dest_keyname)
                return dest_key.set_contents_from_filename(src)
            else:
                src_fns = LS_R(src, base_only=True, ignore_hidden=ignore_hidden)
                src_dirname = BASENAME(src)
                for src_fn in src_fns:
                    full_src_fn = os.path.join(src, src_fn)
                    if dest_path_type == 'dir':
                        dest_keyname = os.path.join(dest_info['keyname'], src_dirname, src_fn)
                    else:
                        dest_keyname = os.path.join(dest_info['keyname'], src_fn)
                    dest_key = dest_info['bucket'].new_key(dest_keyname)
                    dest_key.set_contents_from_filename(full_src_fn)
                return 

        if src_loc_type in ('s3', 's3c') and dest_loc_type == 'fs':
            if src_path_type == 'file':
                if dest_path_type == 'file':
                    if checksize and os.path.exists(dest) and (src_info['key'].size == filesize(dest)):
                        return
                    RM(dest)
                    MKDIR(DIRNAME(dest))
                else:
                    dest = os.path.join(dest, BASENAME(src))
                    if checksize and os.path.exists(dest) and (src_info['key'].size == filesize(dest)):
                        return
                return src_info['key'].get_contents_to_filename(dest)
            else:
                if dest_path_type == 'file':
                    if os.path.exists(dest):
                       raise ValueError('Cannot copy a dir into a single file: %s' % dest)
                prefix = src_info['keyname']
                if not prefix.endswith('/'):
                    prefix = prefix + '/'
                prefix_n = len(prefix)
                src_dirname = BASENAME(src)
                for key in src_info['keys']:
                    if dest_path_type == 'dir':
                        final_dest = os.path.join(dest, src_dirname, key.name[prefix_n:])
                    else:
                        final_dest = os.path.join(dest, key.name[prefix_n:])
                    if checksize and os.path.exists(final_dest) and (src_info['key'].size == filesize(final_dest)):
                        continue
                    MKDIR(DIRNAME(final_dest))
                    key.get_contents_to_filename(final_dest)
                return

        if src_loc_type in ('s3', 's3c') and dest_loc_type == src_loc_type:
            # TODO: add filesize check
            if src_path_type == 'file':
                if dest_path_type == 'dir':
                    dest_keyname = os.path.join(dest_info['keyname'], BASENAME(src_info['keyname']))
                else:
                    dest_keyname = dest_info['keyname']
                return src_info['key'].copy(dest_info['bucketname'], dest_keyname)
            else:
                prefix = src_info['keyname']
                src_dirname = BASENAME(prefix)
                if not prefix.endswith('/'):
                    prefix = prefix + '/'
                prefix_n = len(prefix)
                for key in src_info['keys']:
                    if dest_path_type == 'dir':
                        dest_keyname = os.path.join(dest_info['keyname'], src_dirname, key.name[prefix_n:])
                    else:
                        dest_keyname = os.path.join(dest_info['keyname'], src_dirname, key.name[prefix_n:])
                    key.copy(dest_info['bucketname'], dest_keyname)
                return

        raise NotImplemented('Copying between %s and %s is not implemented yet' % (src_loc_type, dest_loc_type))

    def rm_one_file(self, fn):
        if os.path.islink(fn):
            os.unlink(fn)
        elif os.path.isdir(fn):
            shutil.rmtree(fn)
        else:
            os.remove(fn)

    def force_rm(self, fname, regex=False):
        """force to rm fname, no matter whether fname is file, dir or link"""
        # TODO, add regex support. and make it more robust.
        fname = expand_path(fname)
        loc_type , path_type, info = self.parse_path(fname)

        if loc_type == 'fs':
            if regex:
                top = DIRNAME(fname)
                fname = BASENAME(fname)
                if top == '':
                    top = '.'

                top_fnames = LS(top, fname_pattern=fname)
                for fn in top_fnames:
                    try:
                        self.rm_one_file(os.path.join(top, fn))
                    except Exception as e:
                        print e
            else:
                fnames = glob(fname)
                for fn in fnames:
                    try:
                        self.rm_one_file(fn)
                    except Exception as e:
                        print e

        if loc_type in ('s3', 's3c'):
            if path_type == 'file':
                info['key'].delete()
            else:
                for key in info['keys']:
                    key.delete()
    
    def du(self, top, unit='B'):
        """disk usage
        part of this code comes from monkut (http://stackoverflow.com/questions/1392413/calculating-a-directory-size-using-python)
        """
        total_size = 0
        loc_type, path_type, info = self.parse_path(top)
        if loc_type == 'fs':
            if path_type == 'dir':
                for dirpath, dirnames, filenames in os.walk(top):
                    total_size += os.path.getsize(dirpath)
                    for f in filenames:
                        fp = os.path.join(dirpath, f)
                        total_size += os.path.getsize(fp)
            else:
                total_size = os.path.getsize(top)
        else:
            raise NotImplemented
        return total_size


fileop = FileOperator()
force_rm = fileop.force_rm
CP = fileop.cp
RM = fileop.force_rm
DU = fileop.du

def batch_cp(arglist, cpu_number=1, quiet=False, new_process=False, checksize=False):
    if cpu_number == 1:
        for src, dest in arglist:
            try:
                if not quiet:
                    print "Copying %s => %s" % (src, dest)
                CP(src, dest, checksize=checksize)
            except Exception as e:
                print "!!! Warning: error when copying %s => %s\n%s" % (src, dest, e)
    else:
        if new_process:
            tmp_list_fname = '/tmp/batch_cp.%s.list' % (datetime.now().strftime('%Y%m%d%H%M%S.%f'),)
            with open(tmp_list_fname, 'w') as tmp_list_f:
                for src, dest in arglist:
                    tmp_list_f.write('%s %s\n' % (src, dest))
            cmd = 'batch_cp %s %d quiet=%s checksize=%s' % (tmp_list_fname, cpu_number, quiet, checksize)
            print cmd
            os.system(cmd)

            RM(tmp_list_fname)
        else:
            split_job(batch_cp, arglist, cpu_number=cpu_number, func_kwargs={'cpu_number': 1, 'quiet':quiet, 'checksize':checksize})

#def CP(src, dest, symlinks=True, ignore=None):
#    true_dest = find_link_orig(dest)
#    if os.path.isdir(src):
#        if os.path.isdir(true_dest):
#            dest = os.path.join(dest, os.path.basename(src))
#        else:
#            RM(dest)
#        return shutil.copytree(src, dest)
#    else:
#        if not os.path.isdir(true_dest):
#            RM(dest)
#        return shutil.copy(src, dest)

def strip_ext(path):
    """strips .ext from a path"""
    return os.path.splitext(path)[0]

def get_ext(path):
    """get .ext from a path"""
    return os.path.splitext(path)[1]

def sub_ext(orig, new_ext):
    """sub .ext with a new one"""
    if not new_ext.startswith('.'):
        new_ext = '.' + new_ext
    return strip_ext(orig) + new_ext
