#!/usr/bin/env python

from datetime import datetime, timedelta
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from .parser import *

__all__ = ['datetime_range', 'TR']

def datetime_range(beg, end=None, tdelta='1h'):
    """Returns a list of datetimes from beg to end with tdelta.
    """
    if end is None:
        tokens = beg.split(':')
        beg = tokens[0]
        end = tokens[1]
        if len(tokens) >= 3:  # beg:end:td
            tdelta = tokens[2]

    if not isinstance(beg, datetime):
        beg = parse_datetime(beg)
    if not isinstance(end, datetime):
        end = parse_datetime(end)
    if not isinstance(tdelta, (timedelta, relativedelta)):
        tdelta = parse_timedelta(tdelta)
    result = []
    now = beg
    while now < end:
        result.append(now)
        now = now + tdelta
    return result

TR = datetime_range

if __name__ == "__main__":
    print datetime_range(20110101, 20110105, '1d')
